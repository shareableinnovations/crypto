const crypto = require('crypto');

class InvalidKeyError extends Error {}

function checkKey(key) {
  if (!key) {
    throw new InvalidKeyError();
  }

  return true;
}

module.exports.Crypto = class Crypto {

  constructor(key) {
    checkKey(key);

    this.key = key;
  }

  encrypt(candidate) {
    checkKey(this.key);

    candidate = typeof candidate !== 'string' ? JSON.stringify(candidate) : candidate;

    const encriptionKey = this.key,
      cipher = crypto.createCipher('aes-256-ctr', encriptionKey);

    let crypted = cipher.update(candidate, 'utf8', 'hex');
    crypted += cipher.final('hex');

    return crypted;
  }

  decrypt(candidate) {
    checkKey(this.key);

    const encriptionKey = this.key,
      decipher = crypto.createDecipher('aes-256-ctr', encriptionKey);

    let dec = decipher.update(candidate, 'hex', 'utf8');
    dec += decipher.final('utf8');

    return dec;
  }

};

module.exports.Hash = class Hash {

  static sha256(candidate) {
    return crypto.createHash('sha256').update(candidate, 'latin1').digest('hex');
  }

};
